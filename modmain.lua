local State = GLOBAL.State
local require = GLOBAL.require
SoundEmitter = GLOBAL.SoundEmitter
local FRAMES = GLOBAL.FRAMES
local EventHandler = GLOBAL.EventHandler
local TimeEvent = GLOBAL.TimeEvent
local ActionHandler = GLOBAL.ActionHandler
local ACTIONS = GLOBAL.ACTIONS
local Vector3 = GLOBAL.Vector3
local SpawnPrefab = GLOBAL.SpawnPrefab
--
local _G = GLOBAL
local PREFAB_SKINS = _G.PREFAB_SKINS
local PREFAB_SKINS_IDS = _G.PREFAB_SKINS_IDS
local SKIN_AFFINITY_INFO = GLOBAL.require("skin_affinity_info")

local prefabs = {
	"winkles",
	"winkles_none", 
}
PrefabFiles = prefabs

Assets = 
{
	Asset( "IMAGE", "images/avatars/avatar_winkles.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_winkles.xml" ),
    Asset( "IMAGE", "images/avatars/avatar_ghost_winkles.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_winkles.xml" ),
	Asset( "IMAGE", "images/avatars/self_inspect_winkles.tex" ),
    Asset( "ATLAS", "images/avatars/self_inspect_winkles.xml" ),
    Asset( "IMAGE", "bigportraits/winkles.tex" ),
    Asset( "ATLAS", "bigportraits/winkles.xml" ),
	Asset( "IMAGE", "bigportraits/winkles_none.tex" ),
    Asset( "ATLAS", "bigportraits/winkles_none.xml" ),
    Asset( "IMAGE", "images/map_icons/winkles.tex" ),
    Asset( "ATLAS", "images/map_icons/winkles.xml" ),
	Asset( "IMAGE", "images/names_gold_winkles.tex" ),
    Asset( "ATLAS", "images/names_gold_winkles.xml" ),
	Asset( "IMAGE", "images/names_winkles.tex" ),
    Asset( "ATLAS", "images/names_winkles.xml" ),
	Asset("SOUNDPACKAGE", "sound/weerclops.fev"),
	Asset("SOUND", "sound/weerclops.fsb"),
}

modimport("scripts/strings.lua")
-----------------------------------
--DEERCLOPS: Here's where we'll put all the necessary main code, below tuning

--Tuning
--TUNING.WEERCLOPS_SANITYSTART = GetModConfigData("weer_sanitydrainstart")
--TUNING.WEERCLOPS_FREEZING_KILL_TIME = GetModConfigData("weer_freezekill")
--TUNING.WEERCLOPS_OVERHEAT_KILL_TIME = GetModConfigData("weer_hotkill")
--TUNING.WEERCLOPS_STRUCTURE_SANITY = GetModConfigData("weer_sanitybonus")
--

AddMinimapAtlas("images/map_icons/winkles.xml")

-----------------------------------
--General:
--
TUNING.WINKLES_HEALTH = 125
TUNING.WINKLES_HUNGER = 150
TUNING.WINKLES_SANITY = 175
--TUNING.GAMEMODE_STARTING_ITEMS.DEFAULT.WINKLES = { "" }
--

--Skins api
modimport("scripts/tools/skins_api")

SKIN_AFFINITY_INFO.winkles = {
--	"",
--	"",
--	"",
}

PREFAB_SKINS["winkles"] = {
	"winkles_none", 
--	"",
--	"",
--	"",
}

AddSkinnableCharacter("winkles")

PREFAB_SKINS_IDS = {}
for prefab,skins in pairs(PREFAB_SKINS) do
    PREFAB_SKINS_IDS[prefab] = {}
    for k,v in pairs(skins) do
      	  PREFAB_SKINS_IDS[prefab][v] = k
    end
end

--[[Reforge
if GLOBAL.TheNet:GetServerGameMode() == "lavaarena" then
TUNING.LAVAARENA_STARTING_HEALTH.WOOSE = 125
TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.WOOSE = { "pithpike", "featheredtunic" }
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.WOOSE = 2
end
if GLOBAL.TheNet:GetServerGameMode() == "quagmire" then
TUNING.GAMEMODE_STARTING_ITEMS.QUAGMIRE.WOOSE = { "nesting_woose" }
TUNING.STARTING_ITEM_IMAGE_OVERRIDE["nesting_woose"] = 
{
    atlas = "images/map_icons/nest_woose.xml",
    image = "nest_woose.tex",
}
end
--]]

AddModCharacter("winkles", "ROBOT")

--[[ Credits to RPP. Don't repost, thanks! ]]
