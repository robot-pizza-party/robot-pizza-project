# "Robot Pizza Project"

The general goal: "a character being absolutely bizarre and out there but still fun and cohesive".

Download the current copy by selecting the download button (the bracket with the down pointing arrow after 'WEB IDE' & 'Find file') and select zip to download the mod!
Add the folder in the zip to your DST mods folder (C:\Program Files (x86)\Steam\SteamApps\common\Don't Starve Together\mods) to get play testing!

![image](https://i.imgur.com/jrZ5sn3.png)
