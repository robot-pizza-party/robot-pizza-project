local STRINGS = GLOBAL.STRINGS
local require = GLOBAL.require
----
RemapSoundEvent( "dontstarve/characters/weerclops/talk_LP", "weerclops/weerclops/talk_LP" )
RemapSoundEvent( "dontstarve/characters/weerclops/ghost_LP", "weerclops/weerclops/ghost_LP" )
RemapSoundEvent( "dontstarve/characters/weerclops/hurt", "weerclops/weerclops/hurt" )
RemapSoundEvent( "dontstarve/characters/weerclops/death_voice", "weerclops/weerclops/death_voice" )
RemapSoundEvent( "dontstarve/characters/weerclops/emote", "weerclops/weerclops/emote" )
RemapSoundEvent( "dontstarve/characters/weerclops/yawn", "weerclops/weerclops/yawn" )
RemapSoundEvent( "dontstarve/characters/weerclops/eye_rub_vo", "weerclops/weerclops/eye_rub_vo" )
RemapSoundEvent( "dontstarve/characters/weerclops/pose", "weerclops/weerclops/pose" )
RemapSoundEvent( "dontstarve/characters/weerclops/carol", "weerclops/weerclops/carol" )
RemapSoundEvent( "dontstarve/characters/weerclops/sinking", "weerclops/weerclops/sinking" )
----

--Deerclops
GLOBAL.STRINGS.CHARACTER_TITLES.winkles = "PLACEHOLDER"
GLOBAL.STRINGS.CHARACTER_NAMES.winkles = "PLACEHOLDER"
GLOBAL.STRINGS.CHARACTER_DESCRIPTIONS.winkles = "*PLACEHOLDER\n*PLACEHOLDER\n*PLACEHOLDER"
GLOBAL.STRINGS.CHARACTERS.WINKLES = require "speech_winkles"
GLOBAL.STRINGS.NAMES.WINKLES = "Weerclops"
GLOBAL.STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.winkles = "*PLACEHOLDER\n\n\n\nExpertise:\nMelee, Darts"
GLOBAL.STRINGS.QUAGMIRE_CHARACTER_DESCRIPTIONS.winkles = "*PLACEHOLDER\n\n*Expertise:\nFarming"
GLOBAL.STRINGS.SKIN_NAMES.winkles_none = "PLACEHOLDER"
GLOBAL.STRINGS.SKIN_DESCRIPTIONS.winkles_none = "PLACEHOLDER"
STRINGS.CHARACTER_ABOUTME.winkles = "PLACEHOLDER"
STRINGS.CHARACTER_SURVIVABILITY.winkles = "Slim"

--Skins

--

--Character interaction strings
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WILLOW.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WENDY.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WX78.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WOODIE.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WINKLES = 
{
	        GENERIC = "PLACEHOLDER",
	        ATTACKER = "PLACEHOLDER",
	        MURDERER = "PLACEHOLDER",
	        REVIVER = "PLACEHOLDER",
	        GHOST = "PLACEHOLDER",
	        FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WEBBER.DESCRIBE.WINKLES = 
{
	        GENERIC = "PLACEHOLDER",
	        ATTACKER = "PLACEHOLDER",
	        MURDERER = "PLACEHOLDER",
	        REVIVER = "PLACEHOLDER",
	        GHOST = "PLACEHOLDER",
	        FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WINONA.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
			FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WORTOX.DESCRIBE.WINKLES =
 {
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
            FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WORMWOOD.DESCRIBE.WINKLES = 
{
            GENERIC = "PLACEHOLDER",
            ATTACKER = "PLACEHOLDER",
            MURDERER = "PLACEHOLDER",
            REVIVER = "PLACEHOLDER",
            GHOST = "PLACEHOLDER",
            FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WARLY.DESCRIBE.WINKLES =
 {
		    GENERIC = "PLACEHOLDER",
	        ATTACKER = "PLACEHOLDER",
	        MURDERER = "PLACEHOLDER",
	        REVIVER = "PLACEHOLDER",
	        GHOST = "PLACEHOLDER",
	        FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WURT.DESCRIBE.WINKLES =
 {
		    GENERIC = "PLACEHOLDER",
	        ATTACKER = "PLACEHOLDER",
	        MURDERER = "PLACEHOLDER",
	        REVIVER = "PLACEHOLDER",
	        GHOST = "PLACEHOLDER",
	        FIRESTARTER = "PLACEHOLDER",
}
STRINGS.CHARACTERS.WALTER.DESCRIBE.WINKLES =
 {
		    GENERIC = "PLACEHOLDER",
	        ATTACKER = "PLACEHOLDER",
	        MURDERER = "PLACEHOLDER",
	        REVIVER = "PLACEHOLDER",
	        GHOST = "PLACEHOLDER",
	        FIRESTARTER = "PLACEHOLDER",
}

--SWC
    if GLOBAL.STRINGS.CHARACTERS.WALANI then
GLOBAL.STRINGS.CHARACTERS.WALANI.DESCRIBE.WINKLES = 
{
	        GENERIC = "PLACEHOLDER",
	        ATTACKER = "PLACEHOLDER",
	        MURDERER = "PLACEHOLDER",
	        REVIVER = "PLACEHOLDER",
	        GHOST = "PLACEHOLDER",
	        FIRESTARTER = "PLACEHOLDER",
}
end
    if GLOBAL.STRINGS.CHARACTERS.WOODLEGS then
GLOBAL.STRINGS.CHARACTERS.WOODLEGS.DESCRIBE.WINKLES = 
{
	        GENERIC = "PLACEHOLDER",
	        ATTACKER = "PLACEHOLDER",
	        MURDERER = "PLACEHOLDER",
	        REVIVER = "PLACEHOLDER",
	        GHOST = "PLACEHOLDER",
	        FIRESTARTER = "PLACEHOLDER",
}
end

--[[ Don't repost this mod. Thanks! ]]
