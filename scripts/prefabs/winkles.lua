local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
		Asset("SCRIPT", "scripts/prefabs/player_common.lua"),

		Asset("ANIM", "anim/winkles.zip"),
	    Asset("ANIM", "anim/ghost_winkles_build.zip" ),
}

local start_inv =
{
    default =
    {},
}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.WINKLES
end
local prefabs = FlattenTree(start_inv, true)
	
local function onbecamehuman(inst)
end

local function OnSave(inst, data)

end

local function OnLoad(inst, data)

end

local function ondeath(inst)
    inst:DoTaskInTime(2,function()
        local deadtree = SpawnPrefab("pinecone_sapling")	
        if deadtree ~= nil then
            local x, y, z = inst.Transform:GetWorldPosition()
            local deadtreefx = SpawnPrefab("deer_ice_burst")
	            deadtreefx.Transform:SetPosition(x, y, z)
                deadtree.Transform:SetPosition(x, y, z)
		end
	end)
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon( "weerclops.tex" )
	if TheNet:GetServerGameMode() == "quagmire" then
		inst.regorged = true--TODO

	else

	end
end

local forge_fn = function(inst)
    event_server_data("lavaarena", "prefabs/weerclops").master_postinit(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"staves", "books"})--TODO
end

local master_postinit = function(inst)
    inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

	inst.soundsname = "weerclops"

	inst.components.health:SetMaxHealth(TUNING.WINKLES_HEALTH)
	inst.components.hunger:SetMax(TUNING.WINKLES_HUNGER)
	inst.components.sanity:SetMax(TUNING.WINKLES_SANITY)

	inst.components.foodaffinity:AddPrefabAffinity("watermelonicle", TUNING.AFFINITY_15_CALORIES_SMALL)--Temp

	inst:ListenForEvent("death", ondeath)

	if TheNet:GetServerGameMode() == "lavaarena" then
        inst.forge_fn = forge_fn
		return
    end

	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
	inst.OnNewSpawn = onbecamehuman

	inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)

end

return MakePlayerCharacter("winkles", prefabs, assets, common_postinit, master_postinit)
