local prefabs = {}
table.insert(prefabs, CreatePrefabSkin("winkles_none",{
	base_prefab = "winkles",
	build_name_override = "winkles",
	type = "base",
	rarity = "Character",
	skins = {
		normal_skin = "winkles",
		ghost_skin = "ghost_winkles_build",
	},
	assets = {
		Asset( "ANIM", "anim/winkles.zip" ),
		Asset( "ANIM", "anim/ghost_winkles_build.zip" ),
	},
	skin_tags = { "BASE", "WINKLES", },
	torso_tuck_builds = { "winkles", },
	torso_untuck_wide_builds = { "winkles", },
	has_alternate_for_body = { "winkles", },
	skip_item_gen = true,
	skip_giftable_gen = true,
}))
--[[ TODO
table.insert(prefabs, CreatePrefabSkin("weerclops_ice",
{
	base_prefab = "weerclops",
	build_name_override = "weerclops_ice",
	type = "base",
	rarity = "Elegant",
	rarity_modifier = "Woven",
	skip_item_gen = true,
	skip_giftable_gen = true,
	skin_tags = { "BASE", "WEERCLOPS", "ICE"},
	torso_tuck_builds = { "weerclops_ice", },
	torso_untuck_wide_builds = { "weerclops_ice", },
	has_alternate_for_body = { "weerclops_ice", },
	skins = {
		normal_skin = "weerclops_ice",
		ghost_skin = "ghost_weerclops_build",
	},
	assets = {
		Asset( "ANIM", "anim/weerclops_ice.zip" ),
		Asset( "ANIM", "anim/ghost_weerclops_build.zip" ),
	},

}))--]]
return unpack(prefabs)
